/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author quasi201
 */
public class Casa {
    private String dir;
    private short pisos;
    
    public Casa(){
        
    }
    
    public Casa(String dir, short pisos){
        this.dir=dir;
        this.pisos=pisos;
    }

    /**
     * @return the dir
     */
    public String getDir() {
        return dir;
    }

    /**
     * @param dir the dir to set
     */
    public void setDir(String dir) {
        this.dir = dir;
    }

    /**
     * @return the pisos
     */
    public short getPisos() {
        return pisos;
    }

    /**
     * @param pisos the pisos to set
     */
    public void setPisos(short pisos) {
        this.pisos = pisos;
    }

    @Override
    public String toString() {
        return "Casa{" + "dir=" + dir + ", pisos=" + pisos + '}';
    } 
}
