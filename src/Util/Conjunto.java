/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 *
 * @author quasi201
 * @param <T>
 */
public class Conjunto<T> {
    private Caja<T> cajas[];
    private int i=0;
    
    public Conjunto(){}
    
    public Conjunto(int cantCajas){
        if(cantCajas<=0)
            throw new RuntimeException("No se pueden crear espacios");
   
        this.cajas=new Caja[cantCajas];
    }
    
    public void adicionarElemento(T nuevo)throws Exception{
        if(this.i>=cajas.length)throw new Exception("No se pudo adiccionar el elemento");
        if(this.existeElemento(nuevo))throw new Exception("No se pudo adicionar, el elemento repetido");
        
        this.cajas[i] = new Caja(nuevo);
        i++;
    }
    
    public boolean existeElemento(T nuevo){
        for (int j = 0; j<i; j++) {
            T x = this.cajas[j].getObjeto();
            if(x.equals(nuevo)) return true;
        }
        return false;
    }
    
    public int indexOf(T objBuscar){
        for (int j = 0; j < this.i; j++) {
            if((cajas[j].getObjeto()).equals(objBuscar))
                return j;
        }
        return -1;
    }
    
    public void ordenar(){
        int menor=0;
        for (int j = 0; j <getCapacidad(); j++) {
            menor=j;
            for (int k = j+1; k <getCapacidad(); k++) {
                if((Integer)get(k)<(Integer)get(menor))
                    menor=k;
            }
            if(j!=menor){
                T aux=get(j);
                set(j,get(menor));
                set(menor,aux);
            }
        }
    }
    
    public void ordenarBurbuja(){
        for (int j = 0; j <getCapacidad(); j++) {
            for (int k = 0; k < getCapacidad()-1-j; k++) {
                if((Integer)get(k+1)<(Integer)get(k)){
                    T aux=get(k+1);
                    set(k+1,get(k));
                    set(k,aux);
                }
            }
        }
    }
    
    public void remover(T objBorrado)throws Exception{
        int ind=indexOf(objBorrado);
        if(ind<0)throw new Exception("Objeto a eliminar no encontrado:(");
        else{
            for(int k=ind; k<getCapacidad()-1; k++){
                T aux=get(k);
                set(k,get(k+1));
                set(k+1,aux);
            }
            cajas[this.i-1]=null;
            this.i--;
        }
    }
    public void concatenar(Conjunto<T> nuevo){
        int t = nuevo.cajas.length + this.cajas.length;
        Caja<T> aux[] = new Caja[t];
        int i = 0;
        for(i = 0; i < getCapacidad(); i++)
            aux[i] = this.cajas[i];

        for(int a = 0; a < getCapacidad()-1; a++){
            aux[i] = nuevo.cajas[a];
            i++;
        }    
        this.cajas=aux;
        //nuevo.removeAll();
    }
    
    public void concatenarRestrictivo(Conjunto<T> nuevo)throws Exception{
        for (int j = 0; j < this.getCapacidad(); j++) {
            for (int k = 0; k <nuevo.getCapacidad(); k++) {
                if(this.get(j).equals(nuevo.get(k)))
                    nuevo.remover(nuevo.get(k));
            }
        }
        this.concatenar(nuevo);
    }
    
    public void removeAll(){
        this.cajas=null;
        this.i=0;
    }
    
    @Override
    public String toString(){
        String msg="*****CONJUNTO*****\n";
        for (int j = 0; j <cajas.length && cajas[j]!=null; j++) {
            msg+=cajas[j].getObjeto().toString()+" ";
        }
        /**for (Caja c : cajas) {
            if(c!=null)
            msg+=c.getObjeto().toString()+"\n";
        }*/
        return msg;
    }
    
    public int getCapacidad(){
        return this.i;
    }
    
    public int getLength(){
        return this.cajas.length;
    }
    
    public T get(int indice){
        if(indice<0 || indice>=cajas.length)throw new RuntimeException("Indice fuera de rango");
        
        return cajas[indice].getObjeto();
    }
    
    public void set(int indice, T nuevo){
        if(indice<0 || indice>=cajas.length)throw new RuntimeException("Indice fuera de rango");
        this.cajas[indice].setObjeto(nuevo);
    }
}
