/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Modelo.*;
import Util.Caja;

/**
 *
 * @author quasi201
 */
public class TestGenericidad {
    public static void main(String[] args) {
        Casa cas1 = new Casa("Guaimaral",(short)4);
        Estudiante es = new Estudiante(1151902, "Felipe");
        String msg = "Bienvenidos a la clase de genericidad";
        Integer num = 12345;
        Estudiante es1 = new Estudiante(1151902, "Marco");
        //imprimir(cas1);
        //imprimir(es);
        //imprimir(msg);
        //comparar(cas1,es);
        Caja<Estudiante> c1 = new Caja(es);
        Caja<Estudiante> c2 = new Caja(es1);
        Caja<Casa> c3 = new Caja(cas1);
        Caja<Integer> c4 = new Caja(num);
        
        Caja v[] =  new Caja[4];
        v[0]=c1;
        v[1]=c2;
        v[2]=c3;
        v[3]=c4;
        
        for (Caja c : v) {
            System.out.println(c.toString());
        }
    }
    
    
    public static void imprimirCaja(Caja c){
        System.out.println(c.toString());
    }
    
    public static <T> void imprimir(T objeto){
        System.out.println(objeto.toString());
    }
    
    public static <T> void comparar(T obj1, T obj2){
        int comparacion = ((Comparable)obj1).compareTo(obj2);
        if(comparacion == 0)System.out.println("Son iguales");
        else if(comparacion<0)System.out.println("obj1 es menor que obj2");
        else System.out.println("obj1 es mayor que obj2");
    } 
    
    
    /**
     * Lo que se por el momento es:
     * 
     * -->Al imprimir el toma el toString sobreEscrito que es el del objeto q se pasa por parametro
     * -->Al implementar una interface(implements) debo implementar uno o mas metodos de dicha interface
     * -->Parametrizar es llevar un comodin, esto para poder generalizar y reutilizar mas codigo y utilizar 
     *    metodos generales de la clase Object
     */
}
