/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Conjunto;


/**
 *
 * @author quasi201
 */
public class TestNuevoConjunto {
    public static void main(String[] args) {
        
        try {
            Conjunto<Integer> c = new Conjunto(7);
            Conjunto<Integer> c1 = new Conjunto(7);
            Conjunto<Integer> c2 = new Conjunto(10);
            
            c.adicionarElemento(1);
            c.adicionarElemento(2);
            c.adicionarElemento(3);
            c.adicionarElemento(4);
            c.adicionarElemento(5);
            c.adicionarElemento(6);
            c.adicionarElemento(7);
            
            c1.adicionarElemento(8);
            c1.adicionarElemento(9);
            c1.adicionarElemento(10);
            c1.adicionarElemento(4);
            c1.adicionarElemento(5);
            c1.adicionarElemento(12);
            c1.adicionarElemento(100);
            
            c2.adicionarElemento(8);
            c2.adicionarElemento(9);
            c2.adicionarElemento(10);
            c2.adicionarElemento(4);
            c2.adicionarElemento(5);
            c2.adicionarElemento(12);
            c2.adicionarElemento(100);
            c2.adicionarElemento(15);
            c2.adicionarElemento(42);
            c2.adicionarElemento(40);
            
            
            //Impresion de los metodos*/
            c.concatenar(c1);
            System.out.print("\nConcatenacion de Conjunto1 y Conjunto2:\n" + c.toString());
            //********************************************************************
            c1.concatenarRestrictivo(c);
            System.out.print("\nConcatenacion estricta de Conjunto1 y Conjunto2:\n" + c1.toString());
            //********************************************************************
            c2.ordenar();
            System.out.print("\nconjunto 3 ordenado(Seleccion): \n" + c2.toString());
            //********************************************************************
            c2.ordenarBurbuja();
            System.out.print("\nconjunto 3 ordenado(Burbuja): \n" + c2.toString());
            //********************************************************************
            
            c2.remover(100);
            System.out.println("\nEliminacion del elemento: 100  del Conjunto1: \n"+c.toString());
            
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
